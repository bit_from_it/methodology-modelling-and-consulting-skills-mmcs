!	Authors: Michail Liarmakopoulos, Shuai Chen, Zirui Wang
!	Edinburgh, Nov. 2011
!
!	MMCS Project 1 : Berry Farm Ice Cream
!------------------------------------------------
model BerryFarmExtended
uses "mmxprs"

declarations
	Months = 1..12
	Years = 1..3	
 	Products: 				set of string
 	DemandProducts: 		set of string
 	ContractProducts:		set of string 	
 	Ingredients:  			set of string
 	!
 	MonthName:				array(Months) 		of string
 	ProductPrice:			array(Products) 	of real
 	IngredientCost: 		array(Ingredients) 	of real
 	!
 	ProductionCapacity: 	integer
 	WarehouseSize:			integer
 	InitialLoan:			integer
 	StorageCost: 			real
 	Rent:					real
 	RepaymentFraction:		real
 	MinRepaymentRate:		real
 	InterestRate:			real
 	DiscountRate:			real
 	temp:					real
 	!
 	ExpandBin:				boolean	! If ExpandBin = 0, then RentBin = BuyBin = 0.
 	RentBin:				boolean	! If RentBin = 0, then we Buy.
 	SignBin:				boolean 
 	!
 	Availability: 			array(Ingredients) 				of real
 	Proportions:			array(Ingredients, Products) 	of real
 	Demand:  				array(Products, Months) 		of real 	 	
 	Contract:				array(ContractProducts) 		of real
 	!
 	Make:					array(Products, Months, Years) 	of mpvar !in tonnes
 	Sell:					array(Products, Months, Years) 	of mpvar ! --//--
 	Store:					array(Products, Months, Years) 	of mpvar ! --//--
 	Repayment:				array(Months, Years)			of mpvar !in 1000GBP
end-declarations

initializations from "BerryFarmExpansion-K.dat" 
	Products DemandProducts ContractProducts Ingredients MonthName ProductPrice IngredientCost 
	ProductionCapacity WarehouseSize InitialLoan StorageCost Rent RepaymentFraction MinRepaymentRate 
	InterestRate DiscountRate ExpandBin RentBin SignBin Availability Proportions Demand Contract
end-initializations

forward procedure PrintingResults
forward procedure PrintRentingInfo
forward procedure PrintBuyingInfo !RentCalc BuyCalc SignCalc 
forward procedure PrintBaseInfo

! As we'll explain later, we have six scenarios (3*2).
!
! It is wise to isolate the common calculation blocks (constraints, costs and income)
!-----------------------------------------------------------------------------------------------------------------
 
! 1a) Quantity of Ingredients/Month/Year.
   forall(i in Ingredients, m in Months, y in Years)do   	
         IngredientsUsed(i, m, y):= sum(p in Products)Proportions(i, p)*Make(p, m, y)
         IngredientsUsedCS(i, m, y):= IngredientsUsed(i,m,y) <= Availability(i) !Availability Constraint        
   end-do   

! 1b) Total Quantity of Ingredients Used (sum of all years and months)
   forall(i in Ingredients) TotalIngredientsUsed(i):= sum(m in Months, y in Years)IngredientsUsed(i,m,y)
   
! 1c) Total Cost of Ingredients with Discount (-)
!DiscountedIngrPerMY(i,m,y):= IngredientsUsed(i, m, y)*IngredientCost(i)*(1-DiscountRate)^(m-1 + 12*(y-1))
IngredientsCost:=sum(i in Ingredients, m in Months, y in Years) (IngredientsUsed(i, m, y)*IngredientCost(i)*(1-DiscountRate)^(m-1 + 12*(y-1)))

! 2) Total Income with Discount  (+)
!DiscountedIncPerMY(p, m, y) := Sell(p, m, y)*ProductPrice(p)*(1-DiscountRate)^(m-1 + 12*(y-1))
TotalIncome:= sum(p in Products, m in Months, y in Years) (Sell(p, m, y)*ProductPrice(p)*(1-DiscountRate)^(m-1 + 12*(y-1)))

! 3a) Storage Cost per Month per Year with Discount
 forall(m in Months, y in Years) do
     if (y = 1 and m = 1)then
      		StorageCostPerMY(m,y):= sum(p in Products) Make(p, m, y)*StorageCost 
     else
      		if(m = 1) then
      			StorageCostPerMY(m,y):= sum(p in Products)((Store(p, 12, y-1) + Make(p,m,y))*((1-DiscountRate)^(m-1 + 12*(y-1)))*StorageCost)
      		else
	      		StorageCostPerMY(m,y):= sum(p in Products)((Store(p, m-1, y) + Make(p,m,y))*( (1-DiscountRate)^(m-1 + 12*(y-1)) )*StorageCost)
    	  	end-if
     end-if
 end-do
 
! 3b) Total Storage Cost per Month per Year with Discount (-)
TotalStorageCost := sum(m in Months, y in Years)StorageCostPerMY(m, y)

! CHOOSING THE SCENARIO--------------------------------------------------------------------------------------
! If E =Expand, E' =Don't Expand, R =Rent, R' =Don't Rent =Buy, S =Sign, S' =Don't Sign, then
! the scenarios are {ERS, ERS', ER'S, ER'S', E'S, E'S'}.
! To change the scenario, change the boolean variables ExpandBin, RentBin, SignBin in the data file:
! Possible cases:
! E  ,R  ,S   = 1,1,1              
! E  ,R  ,S'  = 1,1,0           
! E  ,R' ,S   = 1,0,1			
! E  ,R' ,S'  = 1,0,0           
! E'     ,S   = 0,  1           
! E'     ,S'  = 0,  0
!---------------------------------------------------------------------------------------
! DO NOT EDIT THE MOSEL FILE !!! JUST EDIT the boolean variables in the data file !              
!------------------------------------------------------------------------------------------------------------
if ExpandBin then
	writeln("EXPANDING")
	! Doubling production and storage capacity !
	  ProductionCapacity2:=2*ProductionCapacity
	  WarehouseSize2:=2*WarehouseSize
	  
	!RENT SCENARIO---------------------------------------------------------------------------------------- 		
	if RentBin then		
			writeln("RENTING")
			
		! 1) Demand Constraint
								
		if SignBin then!---------------SIGN Sub-scenario----						
			writeln("SIGNING")			
   			forall(m in Months, y in Years)do      		         		        	 			
        	 		forall(p in DemandProducts) Sell(p,m,y) <= Demand(p,m)        	 				        	 			        	 	
        	 		forall(p in ContractProducts) do      				      						      						
      					Make(p,m,y) = Contract(p)			
      					Sell(p,m,y) = Contract(p)   	 	
        	 		end-do        	 				        	 			      					        	 		    	 		      				        	 		
   			end-do   			
		else! Don't Sign sub-scenario								      				
      			writeln("NOT SIGNING")
      			forall(p in Products, m in Months, y in Years)do
	         			Sell(p,m,y)<= Demand(p,m)
      			end-do      												    	    												
		end-if

	! 2) Production Capacity Constraints							 
	forall(m in Months, y in Years)do
	   		TotalProductionCS(m,y) := sum(p in Products)Make(p,m,y)
	   		TotalProductionCS(m,y) <= ProductionCapacity2
	end-do

	! 3) Storage Constraints
		forall(m in Months, y in Years) do
			forall (p in Products) do
				if (m =1 and y=1 ) then
					InventoryCS(p, m,y) := Store(p, m,y) = Make(p, m,y) - Sell(p, m,y)			
				elif(m=1) then
					InventoryCS(p, m,y) := Store(p, m,y) = Store(p,12,y-1) + Make(p, m,y) - Sell(p, m,y)
				else
					InventoryCS(p, m,y) := Store(p, m,y) = Store(p, m-1,y) + Make(p, m,y) - Sell(p, m,y)		
				end-if
			end-do
	
			if (m =1 and y=1 ) then
				TotalStorage(m,y):=sum(p in Products) Make(p,m,y)
			elif(m = 1) then
			 	TotalStorage(m,y) := sum(p in Products) (Make(p,m,y)+Store(p, 12,y-1)) !BRACKETS IN SUMS!!!
			else
    			TotalStorage(m,y) := sum(p in Products) (Make(p,m,y)+Store(p, m-1,y))
	
			end-if	
				StorageCS(m,y) := TotalStorage(m,y) <= WarehouseSize2
		end-do				
		
	! 4) Total Cost for RENTing the warehouse   
   	WarehouseRentingCost:=sum(m in Months,y in Years)(Rent*(1-DiscountRate)^(12*(y-1)+m-1))       
   
   ! 5) Total Profit for RENT scenario
   TotalProfitBase := TotalIncome - IngredientsCost - TotalStorageCost
   TotalProfit := TotalProfitBase - WarehouseRentingCost ! In 1000*GBP        
										
	!BUY SCENARIO-----------------------------------------------------------------------------------------					
	else
		writeln("BUYING")
	! 1) Demand constraint	
		if SignBin then												
			writeln("SIGNING")
			forall(m in Months, y in Years)do
      			forall(p in DemandProducts) Sell(p,m,y) <= Demand(p,m)        	 				        	 			        	 	
        	 	forall(p in ContractProducts) do      				      						      						
      					Make(p,m,y) = Contract(p)
      					Sell(p,m,y) = Contract(p)      					        	 	
      			end-do      			
   			end-do
		else
			writeln("NOT SIGNING")
			forall(p in Products, m in Months, y in Years)do
	         			Sell(p,m,y)<= Demand(p,m)
      		end-do					
		end-if
				
	! 2) Production Capacity Constraints							 
		forall(m in Months, y in Years)do
	   			TotalProductionCS(m,y) := sum(p in Products)Make(p,m,y)
	   			TotalProductionCS(m,y) <= ProductionCapacity2
		end-do

	! 3) Storage Constraints
		forall(m in Months, y in Years) do
			forall (p in Products) do
				if (m =1 and y=1 ) then
					InventoryCS(p, m,y) := Store(p, m,y) = Make(p, m,y) - Sell(p, m,y)			
				elif(m=1) then
					InventoryCS(p, m,y) := Store(p, m,y) = Store(p,12,y-1) + Make(p, m,y) - Sell(p, m,y)
				else
					InventoryCS(p, m,y) := Store(p, m,y) = Store(p, m-1,y) + Make(p, m,y) - Sell(p, m,y)		
				end-if
			end-do
	
			if (m =1 and y=1 ) then
				TotalStorage(m,y):=sum(p in Products) Make(p,m,y)
			elif(m = 1) then
			 	TotalStorage(m,y) := sum(p in Products) (Make(p,m,y)+Store(p, 12,y-1)) !BRACKETS IN SUMS!!!
			else
    			TotalStorage(m,y) := sum(p in Products) (Make(p,m,y)+Store(p, m-1,y))
	
			end-if	
				StorageCS(m,y) := TotalStorage(m,y) <= WarehouseSize2
		end-do	
	
	! 4) Repayment Constraints      	
		forall(m in Months, y in Years)do
    		if(m=1 and y=1) then
	    		!At the end of each month:
       			CurrentLoan(m,y):= InitialLoan*(1.0+InterestRate) - Repayment(m,y)
       			RepaymentCS(m,y):= Repayment(m,y)>=InitialLoan*MinRepaymentRate*(1+InterestRate)  
       		elif(m=1) then
    			CurrentLoan(m,y):= CurrentLoan(12,y-1)*(1+InterestRate)-Repayment(m,y)
      			RepaymentCS(m,y):= Repayment(m,y) >= MinRepaymentRate*CurrentLoan(12,y-1)
      		else
	      		CurrentLoan(m,y):= CurrentLoan(m-1,y)*(1+InterestRate)-Repayment(m,y)
      			RepaymentCS(m,y):= Repayment(m,y) >= MinRepaymentRate*CurrentLoan(m-1,y)
      		end-if    
    	end-do   
    
    ! 4b) After 3 years constraint of the repayment.
    	TotalRepaymentCS := Repayment(12,3) >= RepaymentFraction*InitialLoan 
	
	! 4) The total profit for the Base Case is:
		TotalProfitBase := TotalIncome - IngredientsCost - TotalStorageCost					
	
	! 5) Total Profit for BUY case
		TotalProfit:=TotalProfitBase - sum(m in Months,y in Years)Repayment(m,y)*((1-DiscountRate)^(12*(y-1)+m-1))							
	end-if ! (END OF Rent if-then-else loop)
!BASE SCENARIO---------------------------------------------------------------------------------------------	
else	
	! 1) Demand Constraint
	writeln("BASE")		
		if SignBin then!---------------SIGN sub-scenario----
				writeln("SIGNING")						
   				forall(m in Months, y in Years)do      				
        	 		forall(p in DemandProducts) Sell(p,m,y) <= Demand(p,m)        	 				        	 			        	 	
        	 		forall(p in ContractProducts) do      				      						      						
      					Make(p,m,y) = Contract(p)
      					Sell(p,m,y) = Contract(p)      					   	 	
      				end-do        	 				        	 			      					        	 		    	 		      				
   				end-do   									   				
		else! Don't Sign sub-scenario								      				
      			writeln("NOT SIGNING")
      			forall(p in Products, m in Months, y in Years)do
	         		Sell(p,m,y) <= Demand(p,m)
      			end-do      												
		end-if			   		
	! 2) Production Capacity Constraints							 
	forall(m in Months, y in Years)do
	   		TotalProductionCS(m,y) := sum(p in Products)Make(p,m,y)
	   		TotalProductionCS(m,y) <= ProductionCapacity
	end-do

	! 3) Storage Constraints 
		forall(m in Months, y in Years) do
			forall (p in Products) do
				if (m = 1 and y = 1 ) then
					InventoryCS(p, m,y) := Store(p, m,y) = Make(p, m, y) - Sell(p, m,y)
				elif (m = 1) then
				!if ( m = 1 ) then
					InventoryCS(p, m,y) := Store(p, m,y) = Store(p, 12, y-1) + Make(p, m,y) - Sell(p, m,y)
				else
					InventoryCS(p, m,y) := Store(p, m,y) = Store(p, m-1, y) + Make(p, m,y) - Sell(p, m,y)		
				end-if
			end-do
	
			if (m =1 and y=1 ) then
				TotalStorage(m,y):=sum(p in Products) Make(p,m,y)
			elif(m = 1) then
			!if (m = 1) then
			 	TotalStorage(m,y) := sum(p in Products) (Make(p,m,y) + Store(p, 12,y-1)) !BRACKETS IN SUMS!!!
			else
    			TotalStorage(m,y) := sum(p in Products) (Make(p,m,y) + Store(p, m-1,y))
	
			end-if	
			StorageCS(m,y) := TotalStorage(m,y) <= WarehouseSize
		end-do	
	
	! 4) The total profit for the Base Case is:
	TotalProfitBase := TotalIncome - IngredientsCost - TotalStorageCost

	! 5) Total Profit	
	TotalProfit := TotalProfitBase											
		
end-if 		
!-------------------------------------------END OF SCENARIA---------------------------

! MAXIMIZE THE TOTAL PROFIT
maximize(TotalProfit)

! Print Results in Excel File
PrintingResults

! 6) Printing some Info for RENT ,BUY and BASE cases in the terminal
	
	if ExpandBin then
		if (RentBin and SignBin)then
			PrintRentingInfo			
		elif RentBin then
			PrintRentingInfo				
		elif SignBin then			
			PrintBuyingInfo
		else
			PrintBuyingInfo
		end-if
	else 
		PrintBaseInfo
	end-if	

!-------------------------------------------------------------------------------------------------------
!----------------------------------------------PROCEDURES-----------------------------------------------
!-------------------------------------------------------------------------------------------------------

!-(1)--------------PRINTING THE RESULTS INTO CSV FILE------------------------------------------------------------------
procedure PrintingResults
fopen("BerryIceProject.csv", F_OUTPUT)	
	writeln("Total Income is:",",", "�",getsol(TotalIncome),"k")
   	writeln("Total ingredient cost is:",",","�", getsol(IngredientsCost),"k")
	writeln("Total storage cost is:",",", "�",getsol(TotalStorageCost),"k")
   	writeln("Total discounted Profit is:",",", "�",getsol(TotalProfit),"k")
  	forall(y in Years)do
   	write(",,",y,",")
  	end-do
  	writeln("")
   	writeln("You should make  these quantities of ice cream each month of the year:")
   	write("Month")
   	forall(y in Years )do
   		forall(p in Products)do
   			write(",",p)!,"(Tons)")
   		end-do
   	end-do
   	writeln("")
   
 	forall(m in Months)do
       write(MonthName(m),",")
       forall(y in Years)do
       forall(p in Products)do
          write(getsol(Make(p,m,y)),",")
          end-do
       end-do
       writeln(" ")
 	end-do
 	writeln("")
	writeln("You should  sell these quantities of ice cream each month:")
	forall(m in Months)do
       write(MonthName(m),",")
       forall(y in Years)do
       forall(p in Products)do
          write(getsol(Sell(p,m,y)),",")
          end-do
       end-do
       writeln(" ")
	end-do
	writeln("")
	writeln("You should  store these quantities of ice cream each month:")
	forall(m in Months)do
       write(MonthName(m),",")
       forall(y in Years)do
       forall(p in Products)do
          write(getsol(Store(p,m,y)),",")
          end-do
       end-do
       writeln(" ")
	end-do
	writeln("Demand:")
	forall(m in Months)do
       write(MonthName(m),",")
       forall(y in Years)do
       forall(p in Products)do
          write(Demand(p,m),",")
          end-do
       end-do
       writeln(" ")
	end-do
	writeln("Demand not met:")
	forall(m in Months)do
       write(MonthName(m),",")
       forall(y in Years)do              		
           forall(p in DemandProducts) write(Demand(p,m)-getsol(Sell(p,m,y)),",")        	 				        	 			        	 	
           if (SignBin) then
           		forall(p in ContractProducts) write(Contract(p)-getsol(Sell(p,m,y)))		          
           end-if
       end-do
       writeln(" ")
	end-do   
	fclose(F_OUTPUT)
end-procedure

!------------Printing stuff for the Renting Case at the terminal-----

procedure PrintRentingInfo
writeln("")
writeln("Total Income = �", getsol(TotalIncome),"k")
writeln("Total Ingredients cost = �", getsol(IngredientsCost),"k")
writeln("Total Storage cost = �", getsol(TotalStorageCost),"k")
writeln("TotalProfit = �", getsol(TotalProfit),"k")
writeln("Warehouse Renting cost = �", WarehouseRentingCost,"k")   
end-procedure

!-------Printing stuff for the BUY case at the terminal-------

procedure PrintBuyingInfo
	writeln("")		 
   writeln("Total Income = �", getsol(TotalIncome))
   writeln("Total ingredients cost = �", getsol(IngredientsCost))
    writeln("Total storage cost = �", getsol(StorageCost))
   writeln("Total Profit = �", getsol(TotalProfit))  
   writeln("")

   writeln("You should pay each month to the bank:")      
    forall(y in Years,m in Months) do
    	if (getsol(Repayment(m,y)) <> 0) then
    		write(MonthName(m)," Year_",y, "       ", getsol(Repayment(m,y)))
    		writeln("")
    	end-if    	
    end-do    
    writeln("Rest months had zero repayment.")
    	writeln("")
    	writeln("your current loan is:")   
    forall(y in Years, m in Months)do    
    	if (getsol(CurrentLoan(m,y)) <> 0) then
    		write(MonthName(m)," Year_", y,"       ", getsol(CurrentLoan(m,y)))
    		writeln("")
    	end-if    	
    end-do
    writeln("Rest months had zero current loan.")
end-procedure

!------------------Printing for the Base case at the terminal------------------------

procedure PrintBaseInfo
writeln("")
 writeln("-------------------------------")
   writeln("Total Income = �", getsol(TotalIncome),"k")
   writeln("Total ingredients cost = �", getsol(IngredientsCost),"k")
    writeln("Total storage cost = �", getsol(TotalStorageCost),"k")
   writeln("TotalProfit = �", getsol(TotalProfit),"k")
   writeln("")   
 end-procedure

!-------------End of Model-----------------
end-model
